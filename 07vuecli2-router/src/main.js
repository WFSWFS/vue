import Vue from 'vue'
import App from './App'
//导入router
import router from './router/index.js'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  //请求哪个组件就会显示哪个组件
  router,
  //渲染的过程中会执行一条  <router-view/>
  render: h => h(App)
})
// console.log(router)

//前置钩子(守卫):  全局导航守卫(跳转前回调)
router.beforeEach(function (to,from,next) {
  //从什么跳转到哪里 from => to 右边是route 一个个路由 只需要在route里面加东西就可以了
  //解决第一个页面显示错误的问题
  document.title = to.matched[0].meta.title
  // document.title =to.matched[0].name
    console.log("全局路由跳转前")
  // next 也可以用于页面跳转
  next();

})
//后置钩子(hook)   跳转后回调(全局守卫)无需next
router.afterEach(function (同,from) {
  console.log("全局路由跳转后")
})

//路由独享守卫:就往route里面写

//组件内守卫: 就往组件里写  beforeRouteEnter/update/leave

/*
一般步骤:  ★ ★ ★
          先创建.vue 组件 并且export default{}
          创建index.js 组件对应的路由信息  并且 import 组件
          在main.js里导入父组件App.vue和路由router  在new Vue里面使用它
          在父组件App.vue 里必须使用路由<router-view/>
          最终渲染index.react基础语法  里面的 div#app
*/
