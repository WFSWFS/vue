import Vue from 'vue'
import Router from 'vue-router'
//注意导入的时候@/xxx  不是./
/*import HelloWorld from '@/components/HelloWorld'
import HelloWorld2 from '@/components/HelloWorld2'
import HelloWorld3 from '@/components/HelloWorld3'
import User from '@/components/User'*/
//懒加载  一个懒加载对应一个打包后的js文件  ★★★★
const helloWorld = ()=>import('../components/HelloWorld');
//子路由  使用的时候<router-view></router-view>是在父路由的模板里使用的
const hwnew = ()=>import('../components/hwnew');
const hwmessage = ()=>import('../components/hwmessage');

const helloWorld2 = ()=>import('../components/HelloWorld2');
const helloWorld3 = ()=>import('../components/HelloWorld3');
const User = ()=>import('../components/User');
const profile = ()=>import('../components/profile');
//Vue.use()传入插件plugin
Vue.use(Router)

//创建并导出路由对象
export default
              new Router({
                // 去除请求路径中的#  hash=>h5的history

                routes: [
                  //配一个默认显示的
                  {
                    path:'',
                    redirect:"/1"
                  },
                  //一个url渲染一个组件
                  {
                    path: '/1',
                    name: 'HelloWorld',
                    //懒加载   直接写函数也是可以的
                    component:helloWorld,
                    meta:{
                      title:"helloWorld"
                    },
                    //路由独享守卫beforeEnter /leave ...
                    beforeEnter:function(to,from,next){
                      console.log("beforeEnter helloWorld");
                        next()
                    },

                    children:[
                      //加上默认子路由的路径
                    /*  {
                        path:"",
                        redirect:"hwnews"
                      },*/
                      //子路由是不需要加/news 的/的
                      {
                        path:"hwnews",
                        // name:"hwnew",
                        component:hwnew
                      },
                      {
                        path:"hwmessages",
                        // name:"hwmessage",
                        component:hwmessage
                      }
                    ]
                  },

                  {
                    path: '/2',
                    name: 'HelloWorld2',
                    component:helloWorld2,
                    meta:{
                      title:"helloWorld2"
                    }
                  },
                  {
                    path: '/3',
                    name: 'HelloWorld3',
                    component:helloWorld3,
                    meta:{
                      title:"helloWorld3"
                    }
                  },

                  {
                    //这个很像ajax里面的params传参
                    path: '/user/:id',
                    name: 'User',
                    component:User,
                    meta:{
                      title:"User"
                    }
                  },
                  {
                    path: '/profile',
                    name: 'profiles123',
                    component:profile,
                    meta:{
                      title:"profile"
                    }
                  }
                ],
                mode:"history",
                //统一定义按钮点击活跃样式
                linkActiveClass:"active",

})
