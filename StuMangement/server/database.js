
const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/studentm',{
  useNewUrlParser:true,
  useFindAndModify:true,
  useCreateIndex:true,
})

// var Schema = mongoose.Schema;

var Student=new mongoose.Schema({

  snumber:{type:String},
  class:{type:String},
  name:{type:String},
  sex:{type:String},
  clan:{type:String},
  javalan:{type:String},
  dblan:{type:String},

})

module.exports = mongoose.model('Student',Student);