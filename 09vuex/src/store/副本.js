import Vue from 'vue'
import Vuex from 'vuex'
//安装插件
Vue.use(Vuex);

const moduleA = {
  state:{name:"jack"},
  mutations:{
    updateName(state,payload){
      //根据外界传的名字修改
      state.name = payload
    }
  },
  actions:{
    //异步改名  这里的context 只对自己模块的mutations有效
    AupdateName(context){
      // console.log(context)
      setTimeout(()=>{
        context.commit('updateName',"wangwu")
      },2000)

    }
  },
  getters:{
    //如何使用模块的计算属性  像以前一样
      fullname(state){
        return state.name+"宇智波"
      },
      addfullname(state,getters){
        return getters.fullname+"宇智波歹徒"
      },
    //拼接new Vuex.Store里面的state★★★★★ 第三个固定参数只有在模块里才有
      addfinalfullname(state,getters,rootState){
        return getters.addfullname+rootState.count
    }
  },
}
const moduleB = {
  state:{},
  mutations:{},
  actions:{},
  getters:{},
}

const store = new Vuex.Store({

  //放入的东西都是固定的 5个
  //state:放入共享的状态信息(数据)  类似data
  state:{
    //这里写的数据全部都加入到vue响应式系统中了
    count:1000,
    students:[
      {id:110,name:"wf",age:5},
      {id:111,name:"wf2",age:15},
      {id:112,name:"wf3",age:25},
      {id:113,name:"wf4",age:35}
    ],
    info:{
      name:"wfwf",
      age:23,
      height:180
    }
  },
  //定义处理state里面的数据的方法  类似methods
  //状态更新  放的是同步函数(方法)
  mutations:{
    //定义方法  state 式默认有的  可以修改state.count的值
    add(state){
      state.count++
    },
    dec(state){
      state.count--
    },
    //普通写法
  /*  increment(state,counter){
      state.count+=counter
    },*/
    //payload是个对象★★★
    increment(state,payload){
      state.count+=payload.counter
    },
    decrement(state,counter){
      state.count-=counter
    },

    AddStu(state,stu){
      state.students.push(stu)
    },
    //响应式属性,
    //组件 => (dispatch) => actions =>(commit) => mutations
    modifyInf(state){
     state.info.name = "wmz";
   /* 这些都是同步操作
      即使是没有的属性也可以添加的上去
      state.info['address'] = "安徽";
      //添加属性:方法2
      Vue.set(state.info,'area',"滁州");
      //删除属性(delete是响应式的)
      delete state.info.age;
      //第二种删除属性
      Vue.delete(state.info,"name");
    */

   /* setTimeout(function () {
      //异步操作的时候vuex插件不能立刻捕获改之后的结果这个问题必须在actions里解决
      state.info.name = "wmz"
    },1000)*/
    }
  },

  actions:{
  // 异步操作放置在这里  默认参数context(上下文) == 第六行的store对象
  // 默认参数 context,payload是传进来的参数例如'xxx'
    updateInfo(context,payload){
    /*异步操作为什么不能在mutations里面写？因为页面数据虽然刷新但是devtools里面的数据不会变出现了分歧
    本质★★★:异步操作拿外边的数据？

       setTimeout(function () {
       //modifyInf是mutations里面的方法名称
       // 组件 => (dispatch) => actions =>(commit) => mutations
       context.commit('modifyInf');
       //payload参数
       //这里要平行看
       // console.log(payload);
       // payload();
        console.log(payload.message)
        payload.success();
      },1000)

      */
     // 本质★★★:异步操作使用的外面的数据 && 异步操作内部数据传出去给外部使用
       return new Promise((resolve ,reject)=>{
          setTimeout(function () {
            //异步操作的内容
            context.commit('modifyInf');
            console.log(payload);
            resolve("11111")
          },2000)
       })


    }

  },
  getters:{
    //类似计算属性computed  默认属性state,getters且只接受这两个参数
      pingfang(state){
        return state.count**2
      },
    //state不能漏★★★★
      filterStu(state){
        return state.students.filter(s=>s.age>20)
      },
    //注意第二个参数(固定的)  上面的属性值是可以给后面用的getters是上面:前的getters★★★
      filterStuLength(state,getters){
        return getters.filterStu.length
    },
    //封装帅选年龄的接口(函数)
      moreAgeStu(state){
        return function (inputAge) {
            return state.students.filter(s=>s.age>inputAge)
        }
    }
  },

  modules:{
  //模块划分,意思是 虽然不推荐创建多个store,但是可以创建多个模块modules
    //a,b这俩属性会默认增加在state里面★★★★
    a:moduleA,
    b:moduleB
  }


})
//可以把store 一分五个js再导进来
export default store
