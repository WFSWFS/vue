export default {
  state:{name:"jack"},
  mutations:{
    updateName(state,payload){
      //根据外界传的名字修改
      state.name = payload
    }
  },
  actions:{
    //异步改名  这里的context 只对自己模块的mutations有效
    AupdateName(context){
      // console.log(context)
      setTimeout(()=>{
        context.commit('updateName',"wangwu")
      },2000)

    }
  },
  getters:{
    //如何使用模块的计算属性  像以前一样
    fullname(state){
      return state.name+"宇智波"
    },
    addfullname(state,getters){
      return getters.fullname+"宇智波歹徒"
    },
    //拼接new Vuex.Store里面的state
    addfinalfullname(state,getters,rootState){
      return getters.addfullname+rootState.count
    }
  },
}
