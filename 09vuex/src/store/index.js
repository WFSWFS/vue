import Vue from 'vue'
import Vuex from 'vuex'
//安装插件
import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";
import moduleA from "./modules/moduleA/moduleA";//放到modules里就可以了
Vue.use(Vuex);

// const moduleA = moduleA

/*const moduleB = {
  state:{},
  mutations:{},
  actions:{},
  getters:{},
}*/
const state = {
  //响应式数据
  count:1000,
  students:[
    {id:110,name:"wf",age:5},
    {id:111,name:"wf2",age:15},
    {id:112,name:"wf3",age:25},
    {id:113,name:"wf4",age:35}
  ],
  info:{
    name:"wfwf",
    age:23,
    height:180
  }
}
const store = new Vuex.Store({

  //放入的东西都是固定的 5个
  //state:放入共享的状态信息(数据)  类似data
  state:state,
  //定义处理state里面的数据的方法  类似methods
  //状态更新  放的是同步函数(方法)
  mutations:mutations,
  actions:actions,
  getters:getters,

  modules:{
  //模块划分,意思是 虽然不推荐创建多个store,但是可以创建多个模块modules
    //a,b这俩属性会默认增加在state里面★★★★
    a:moduleA
    // b:moduleB
  }
})
//可以把store 一分五个js再导进来
export default store
