export default {
//定义方法  state 式默认有的  可以修改state.count的值
  add(state){
    state.count++
  },
  dec(state){
    state.count--
  },
  //普通写法
  /*  increment(state,counter){
      state.count+=counter
    },*/
  //payload是个对象
  increment(state,payload){
    state.count+=payload.counter
  },
  decrement(state,counter){
    state.count-=counter
  },
  AddStu(state,stu){
    state.students.push(stu)
  },
  //响应式属性,
  //组件 => (dispatch) => actions =>(commit) => mutations
  modifyInf(state){
    state.info.name = "wmz";
    /* 这些都是同步操作
       即使是没有的属性也可以添加的上去
       state.info['address'] = "安徽";
       //添加属性:方法2
       Vue.set(state.info,'area',"滁州");
       //删除属性(delete是响应式的)
       delete state.info.age;
       //第二种删除属性
       Vue.delete(state.info,"name");
     */

    /* setTimeout(function () {
       //异步操作的时候vuex插件不能立刻捕获改之后的结果这个问题必须在actions里解决
       state.info.name = "wmz"
     },1000)*/
  }
}
