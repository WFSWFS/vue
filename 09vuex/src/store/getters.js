export default {
//类似计算属性computed  默认属性state,getters
  pingfang(state){
    return state.count**2
  },
  //state不能漏★★★★
  filterStu(state){
    return state.students.filter(s=>s.age>20)
  },
  //注意第二个参数(固定的)  上面的属性值是可以给后面用的
  filterStuLength(state,getters){
    return getters.filterStu.length
  },
  //封装帅选年龄的接口(函数)
  moreAgeStu(state){
    return function (inputAge) {
      return state.students.filter(s=>s.age>inputAge)
    }
  }
}
