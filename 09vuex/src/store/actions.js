export default {
  //异步操作放置在这里  默认参数context(上下文) == 第六行的store对象
  // 异步操作的时候vuex插件不能立刻捕获改之后的结果这个问题必须在actions里解决
  updateInfo(context,payload){
    /*
    本质★★★:异步操作拿外边的数据？

       setTimeout(function () {
       //modifyInf是mutations里面的方法名称
       // 组件 => (dispatch) => actions =>(commit) => mutations
       context.commit('modifyInf');
       //payload参数
       //这里要平行看
       // console.log(payload);
       // payload();
        console.log(payload.message)
        payload.success();
      },1000)

      */
    // 本质★★★:异步操作使用的外面的数据 && 异步操作内部数据传出去给外部使用
    return new Promise((resolve ,reject)=>{
      setTimeout(function () {
        context.commit('modifyInf');
        console.log(payload);
        resolve("11111")
      },2000)
    })


  }

}
