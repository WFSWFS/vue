第一天:
    1:搭出项目整体结构,四个页面 对应四个路由 
        使用的组件:tab-bar,tab-bar-item main-tab-bar
    2:首页navbar组件的封装及其使用
        使用的组件nav-bar
    3:首页axios请求后台数据,方法封装getHomeMultidata并引入到首页
        request.js,home.js
    4:首页轮播图组件的封装与使用
        使用的组件:Swiper，SwiperItem
    5:首页 RecommendView组件的使用,featureView组件的使用

第二天:
    1:tab-control组件的封装,"实现流行,新款,精选 菜单栏 CSS定位及其点击按钮实现跳转"
    2:axios网络请求封装getHomeGoods方法 并引入到首页
    3:axios获取后台goods数据之后赋值给自己定义的goods对象
        goods: {
          pop: { page: 0, list: [] },
          new: { page: 0, list: [] },
          sell: { page: 0, list: [] }
         }
         在methods里面再次封装网络请求 后在created里面调用
  4: 首页对已经获取的goods数据的展示步骤:
     先将goods下面两个组件GoodsList,GoodsListItem创建在components/content下面
     ①GoodsList需要使用并注册GoodsListItem小组件
     ②首页里面引入GoodsList大组件并注册使用
     此时首页 与GoodsList成为父子关系,GoodsList与GoodsListItem成为父子关系
     两次父传子将数据传至GoodsListItem并展示,进行相关Css布局
     ③:tab-control的菜单栏需要实现不同请求展现不同数据,用到子传父emit一个事件并
     在首页的methods里面实现 本质是改变goods[this.currentType].list 商品列表

第三天:
     better-scroll的安装及其使用:解决移动端屏幕顺滑问题
     scroll 与back-top组件的封装及其使用
     解决问题:核心函数scroll  pullingUp  finishPullUp 都是内置方法
     如何上拉加载数据,多次加载数据,如何使异步加载图片实时显示出来而不是等待
     图片加载完成才显示。如何实现点击图标回到顶部,如何实现图标到达一定位置自动出现与消失
     父子组件通信的细节注意事项... 加不加:的问题以及 this.$refs.scroll的使用
第四天:
     ①解决首页中可滚动区域的问题★★★★★★★★★
     Better-Scroll在决定有多少区域可以滚动时,是根据scrollerHeight属性决定
     scrollerHeight属 性是根据放Better-Scroll的content中的子组件的高度
     但是我们的首页中,刚开始在计算scrollerHeight属性时,是没有将图片计算在内的
     所以,计算出来的告诉是错误的(1300+)
     后来图片加载进来之后有了新的高度,但是scrollerHeight属性并没有进行更新.
     所以滚动出现了问题,滚不动了卡了！！
     如何解决这个问题了?
     监听每- -张图片是否加载完成,只要有- -张图片加载完成了,执行一-次refresh()
     如何监听图片加载完成了?
     原生的js监听图片: img.onload = function() {}
     Vue中监听: @load='方法'
     调用scroll的refresh()
     如何将GoodsListltem.vue中 的事件传入到Home.vue中
     因为涉及到非父子组件的通信,所以这里我们选择了事件总线γ
     bus ->公共汽车/总线  
     Vue.prototype.$bus = new Vue()
     this.bus.emit(‘事件名称', 参数)
     this.bus.on(‘事件 名称,回调函数(参数))
 I   
     ②解决频繁refresh的防抖函数处理
     对于refresh非常频繁的问题,进行防抖操作
     防抖debounce/节流throttle(课下研究- 下)
     防抖函数起作用的过程:    
     如果我们直接执行refresh,那么refresh函数会被执行30次.
     可以将refresh函数传入到debounce函数中,生成- -个新的函数.
     之后在调用非常频繁的时候,就使用新生成的函数.
     而新生成的函数,并不会非常频繁的调用,如果下一次执行来的非常快,那么会将上- -次取消掉
     debounce(func, delay) {
     let timer = null
     return function (...args) {
     if (timer) clearTimeout(timer)
     timer = setTimeout(() => {
     func.apply(this, args)
     }，delay)
     }   
     再次解决上拉加载更多
     tab-control的sticky定位不起作用，重新设置吸顶效果(这里使用两个tab-control形成视觉差)
     只是滚到一定距离隐藏一个，fixed一个,注意他们的currentIndex要保持一致
     tab-control的offsetTop
    
 第五天:
  ①在router外面包裹一层 keep-alive 记住离开的状态(不要第二步好像也行)
  ②还要记录页面离开的y位置 deactivated，回来的时候要在这个位置用到activated
  deactivated和activated都在有keep-alive的情况下才有效
  
  详情页的展示
  点击跳转进对应goodsItem的详情页
  
  
  第六天:cv
  
  
  
  服务器nginx部署
    npm run build(打包)  cv dist文件夹 至 nginx中
    npm i -S nginx(安装服务器)