//插件：css px=> vw 等比例转换
module.exports = {
  plugins: {
      autoprefixer:{},
      "postcss-px-to-viewport": {
      //视窗的宽度，对应的是我们设计稿的宽度．
      viewportWidth: 375,
      //视窗的高度，对应的是我们设计稿的高度.(也可以不配置)
      viewportHeight: 667,
      // 指定 px转换为视窗单位值的小数位数（很多时候无法整除)
      unitPrecision: 5,
      //指定需要转换成的视窗单位，建议使用vw
      viewportUnit: 'vw ',
      //指定不需要转换的类,后面再讲． 不需要转化单位的只需要加一个ignore类
      selectorBlackList:['ignore','tab-bar','tab-bar-item'],
      //小于或等于`1px`不转换为视窗单位.
      minPixelValue: 1,
      // 允许在媒体查询中转换`px
      mediaQuery: false,
      // (正则表达式)里面的 不转换单位
      exclude:[/TabBar/]

      },
    }
  }



