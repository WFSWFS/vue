//自定义插件
import Toast from './Toast'
const obj = {

}
//Vue参数默认
obj.install = function (Vue) {
  //在这里做toastue
  // console.log(Vue)
  // document.body.appendChild(Toast.$el)

  //1创建组件构造器
  const toastCons = Vue.extend(Toast)
  // 2 new 根据组件构造器 创建出来组件对象
  const toast  = new toastCons()
  //3 组件的挂载 至某一个元素
  toast.$mount(document.createElement('div'))
  //4 Toast.$el 对应 div
  document.body.appendChild(toast.$el)

  Vue.prototype.$toast = toast

}

export default obj
