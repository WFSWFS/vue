//自定义插件
import Message from './Message'
const obj2 = {

}
//Vue参数默认 每个插件的   对象.install 方法  对象的命名是不可以重复的
obj2.install = function (Vue) {
  //在这里做toastue
  // console.log(Vue)
  // document.body.appendChild(Toast.$el)

  //1创建组件构造器
  const  messageCons = Vue.extend(Message)
  // 2 new 根据组件构造器 创建出来组件对象
  const message = new messageCons()
  //3 组件的挂载 至某一个元素
  message.$mount(document.createElement('div'))
  //4 Toast.$el 对应 div
  document.body.appendChild(message.$el)

  Vue.prototype.$message = message

}

export default obj2
