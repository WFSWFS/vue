//防抖函数的封装 ★★★★  :  防止频繁调用refresh  防止一个函数过多的重复执行
//在事件被触发n秒后再执行回调，如果在这n秒内又被触发，则重新计时。

export function  debounce(func,delay){
  // let timer = null;
  let timer ;
  return function (...args) {
    // var args = arguments
    if (timer){
      clearTimeout(timer)
    }
    timer = setTimeout(()=>{
      //给this添加临时方法func
      //   console.log(this)   不apply也行
      //   func()
      func.apply(this,args)
    },delay)
  }
}
//第二种防抖函数，先立即执行一次，过N秒之后才能再次立即执行一次...
export function  debounce2(func,delay){
  let timer ;
  return function (...args) {
    // var args = arguments
    if (timer){
      //取消之前的任务
      clearTimeout(timer)
    }
    let callnow = !timer
    //重新生成一个定时器 使timer有值
    timer = setTimeout(()=>{
      timer=null;
    },delay)
    if (callnow){
      func.apply(this,args)
    }
  }

}
//节流函数:每隔一段时间，只执行一次函数。
//如果我们一直在浏览器中移动鼠标（比如10s），则在这10s内会每隔1s执行一次testThrottle，
// 函数节流的目的，是为了限制函数一段时间内只能执行一次
export function throttle(fn, delay) {
  var timer;
  return function () {
    var _this = this;
    var args = arguments;
    //有定时器就不管，没有则创建，第一次是没有的
    if (timer) {
      return;
    }
    //创建定时器
    timer = setTimeout(function () {
      fn.apply(_this, args);
      // 在delay后执行完fn之后清空timer，此时timer为假，throttle触发可以进入计时器
      timer = null;
    }, delay)
  }
}
//节流函数2 利用时间戳
export function throttle2(fn, delay) {
  var previous = 0;
  // 使用闭包返回一个函数并且用到闭包函数外面的变量previous
  return function() {
    var _this = this;
    var args = arguments;
    //当某事件不断触发的时候，now的值会一直变直到...
    var now = new Date();
    //现时间-之前的时间 是否超过设置的delay
    if(now - previous > delay) {
      fn.apply(_this, args);
      previous = now;
    }
  }
}
/*  函数防抖的应用场景
    连续的事件，只需触发一次回调的场景有：
    搜索框搜索输入。只需用户最后一次输入完，再发送请求
    手机号、邮箱验证输入检测
    窗口大小Resize。只需窗口调整完成后，计算窗口大小。防止重复渲染。

    函数节流的应用场景
    间隔一段时间执行一次回调的场景有：
    滚动加载，加载更多或滚到底部监听
    谷歌搜索框，搜索联想功能
    高频点击提交，表单重复提交
    */


//格式化时间格式(直接用就好)经常用！！！！
export function formatDate(date, fmt) {
  //获取年
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
  }
  let o = {
    //?+  全部是正则里面的
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds()
  };
  for (let k in o) {
    if (new RegExp(`(${k})`).test(fmt)) {
      let str = o[k] + '';
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str));
    }
  }
  return fmt;
};
//不足两位数补齐两位 时间看起来更好看
function padLeftZero (str) {
  return ('00' + str).substr(str.length);
};

