import Vue from 'vue'
import VueRouter from 'vue-router'
const Home = ()=>import('@/views/FirPage/FirPage')
const Category = ()=>import('@/views/category/category')
const ShopCar = ()=>import('@/views/shopcar/shopcar')
const profile = ()=>import('@/views/profile/Profile')
//详情页路由
const Detail = ()=>import('@/views/detail/Detail')

Vue.use(VueRouter)

const routes = [
    {
        path: '',
        redirect:"/home"
    },
    {
        path:'/home',
        component:Home
    },
    {
        path:'/category',
        component:Category
    },
    {
        path:'/shopCar',
        component:ShopCar
    },
    {
        path:'/profile',
        component:profile
    },
    {
        name:'Detail',
        //params
        path:'/Detail/:iid',
        component: Detail
    }
]

const router = new VueRouter({
    routes,
    mode: 'history'  // 路由模式
    // base: process.env.BASE_URL,

})

export default router
