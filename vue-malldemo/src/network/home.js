import {request} from "./request";
//所有首页相关的axios请求都在这里
export function getHomeMultidata() {
  return request({
    url: '/home/multidata'
  })
}
// 获取商品的信息
export function getHomeGoods(type, page) {
  return request({
    url: '/home/data',
    params: {
      type,
      page
    }
  })
}

