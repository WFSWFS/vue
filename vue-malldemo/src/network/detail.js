import {request} from './request.js'
//详情页网络请求的封装
//根据商品iid获取商品信息(请求数据的)
export function getDetail(iid){
   return request({
        url:'/detail',
        params:{
           iid
       }
    });
}

//最后请求商品推荐的接口
export function getRecommend(){
    return request({
        url:'/recommend'
        //没有参数
    });
}

//将服务器返回的杂乱数据整合; 抽离数据
/*  由于DetailBaseInfo展示的商品(goods)数据不在一个对象里面(三个对象),
    所以我们自定义一个对象保存基本信息数据*/
export class Goods{
    constructor(itemInfo,columns,services){
        this.title = itemInfo.title;
        this.desc = itemInfo.desc;
        this.newPrice = itemInfo.price;
        this.oldPrice = itemInfo.oldPrice
        this.discount = itemInfo.discountDesc;

        this.columns = columns;
        this.services = services;
        this.realPrice = itemInfo.lowNowPrice;
    }
}
/*export 	function Goods(itemInfo,columns,services){
    this.title = itemInfo.title;
    this.desc = itemInfo.desc;
    this.newPrice = itemInfo.price;
    this.oldPrice = itemInfo.oldPrice
    this.discount = itemInfo.discountDesc;

    this.columns = columns;
    this.services = services;
    this.realPrice = itemInfo.lowNowPrice;
};*/
//自定义店铺(商家)信息数据
export class Shop{
    constructor(shopInfo){
        this.logo = shopInfo.shopLogo;
        this.name = shopInfo.name;
        this.fans = shopInfo.cFans;
        this.sells = shopInfo.cSells;
        this.score = shopInfo.score;
        this.goodsCount = shopInfo.cGoods;
    }
}
//商品的参数信息
export class GoodsParam {
    constructor(info, rule) {
        // 注: images可能没有值(某些商品有值, 某些没有值)
        this.image = info.images ? info.images[0] : '';

        this.sizes = rule.tables;
        this.infos = info.set;
    }
}
// 商品图片详细信息

