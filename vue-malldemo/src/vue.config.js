module.exports = {
    configureWebpack: {
        resolve: {
            alias: {
                //为文件  配置别名(前名后值,键值对) 使用时写后面的
                // '@':'src',已经默认配置
                'assets': '@/assets',
                'common': '@/common',
                'components': '@/components',
                'network': '@/network',
                'views': '@/views',
                'pluginunit':'@/pluginunit'
            }
        }
    },
    devServer: {
        host:   '192.168.0.105',  //本机电脑 ip 地址
        port:  8080 //端口号
    }
}