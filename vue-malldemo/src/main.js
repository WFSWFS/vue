import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//1:移动端300ms延迟问题的解决
import fastClick from 'fastclick'
// 解决 vue图片懒加载问题
 import lazyLoad from 'vue-lazyload'
import toast from './components/common/toast'
Vue.use(toast)
import message from './components/common/message'
//安装toast插件 会执行对应的install函数
Vue.use(message)

//安装toast插件 会执行对应的install函数
// Vue.use(message)


Vue.config.productionTip = false
// 在Vue原型上加一个 $bus用于事件总线  Vue实例(作为总线)才能.emit事件
Vue.prototype.$bus = new Vue()

// 使用lazyLoad 懒加载图片问题
// Vue.use(lazyLoad,{loading:require('暂时顶替的图片')}) 此时前面所有的img 标签的  :src 要全部改成<img v-lazy = '变量名'>
//2:调用attach方法就可以了
fastClick.attach(document.body)

new Vue({
  el:"#app",
  router,
  store,
  render: h => h(App)
})



/*import axios from 'axios'
Vue.prototype.$http = axios.create({
  baseURL: 'http://152.136.185.210:8000/api/w6',
  timeout: 5000
})*/