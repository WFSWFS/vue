import Vue from 'vue'
import Router from 'vue-router'
//懒加载首页,分类,购物车,我的(个人)
const Home = ()=>import('../views/FirPage/FirPage')
const Category = ()=>import('../views/category/category')
const ShopCar = ()=>import('../views/shopcar/shopcar')
const profile = ()=>import('../views/profile/profile')

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '',
      redirect:"/category"
    },
    {
      path:'/home',
      component:Home
    },
    {
      path:'/category',
      component:Category
    },
    {
      path:'/shop',
      component:ShopCar
    },
    {
      path:'/profile',
      component:profile
    }
  ],
  //hash=>history模式
  mode:'history'
})
